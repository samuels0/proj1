package enigma;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import java.util.*;

import net.sf.saxon.expr.Component;
import org.checkerframework.checker.units.qual.A;
import ucb.util.CommandArgs;

import static enigma.EnigmaException.*;

/** Enigma simulator.
 *  @author
 */
public final class Main {

    /** Process a sequence of encryptions and decryptions, as
     *  specified by ARGS, where 1 <= ARGS.length <= 3.
     *  ARGS[0] is the name of a configuration file.
     *  ARGS[1] is optional; when present, it names an input file
     *  containing messages.  Otherwise, input comes from the standard
     *  input.  ARGS[2] is optional; when present, it names an output
     *  file for processed messages.  Otherwise, output goes to the
     *  standard output. Exits normally if there are no errors in the input;
     *  otherwise with code 1. */
    public static void main(String... args) {
        try {
            CommandArgs options =
                    new CommandArgs("--verbose --=(.*){1,3}", args);
            if (!options.ok()) {
                throw error("Usage: java enigma.Main [--verbose] "
                        + "[INPUT [OUTPUT]]");
            }

            _verbose = options.contains("--verbose");
            new Main(options.get("--")).process();
            return;
        } catch (EnigmaException excp) {
            System.err.printf("Error: %s%n", excp.getMessage());
        }
        System.exit(1);
    }

    /** Open the necessary files for non-option arguments ARGS (see comment
     *  on main). */
    Main(List<String> args) {
        _config = getInput(args.get(0));

        if (args.size() > 1) {
            _input = getInput(args.get(1));
        } else {
            _input = new Scanner(System.in);
        }

        if (args.size() > 2) {
            _output = getOutput(args.get(2));
        } else {
            _output = System.out;
        }
    }

    /** Return a Scanner reading from the file named NAME. */
    private Scanner getInput(String name) {
        try {
            return new Scanner(new File(name));
        } catch (IOException excp) {
            throw error("could not open %s", name);
        }
    }

    /** Return a PrintStream writing to the file named NAME. */
    private PrintStream getOutput(String name) {
        try {
            return new PrintStream(new File(name));
        } catch (IOException excp) {
            throw error("could not open %s", name);
        }
    }

    /** Configure an Enigma machine from the contents of configuration
     *  file _config and apply it to the messages in _input, sending the
     *  results to _output. */
    private void process() {
        Machine M = readConfig();
        Scanner curLine;
        String setting = "";

        while (_input.hasNextLine()) {

            if (_input.hasNext("\\s*[*].*")) {
                String nextpart = _input.nextLine();
                curLine = new Scanner(nextpart);

                while (!(curLine.hasNext("\\s*[*].*"))) {
                    printMessageLine(nextpart);
                    curLine = new Scanner(_input.nextLine());
                }
                setting = "";
                for (int i = 0; i <= M.numRotors() + 1; i++) {
                    try {
                        setting = setting.concat(curLine.next() + " ");
                    } catch (Exception e) {
                        System.out.println("Need more rotors");
                    }
                }

                if (curLine.hasNext()) {
                    nextpart = curLine.next();
                }
                while (nextpart.matches("\\s*[(].+[)]")) {
                    setting = setting.concat(nextpart) + " ";
                    if (curLine.hasNext()) {
                        nextpart = curLine.next();
                    } else {
                        nextpart = "";
                    }
                }
            }
            String settingFixed = "";
            for (int i = 0; i < setting.length() - 1; i++) {
                settingFixed = settingFixed.concat(String.valueOf(setting.charAt(i))).toUpperCase(Locale.ROOT);
            }

            setUp(M, settingFixed);

            while (_input.hasNextLine() && !_input.hasNext("\s*[*].*")) {
                String messageLine = _input.nextLine();
                messageLine.replaceAll("\\s+", "");
                String message = M.convert(messageLine);
                printMessageLine(message);
            }
            if (!_input.hasNext("\s*[*].*")) {
            }

        }



    }

    /** Return an Enigma machine configured from the contents of configuration
     *  file _config. */
    private Machine readConfig() {
        ArrayList<Rotor> rotorArrayList = new ArrayList<>();
        int rotors = 0;
        int pawls = 0;

        try {
            if (_config.hasNext("[^ ]+")) {
                _alphabet = new Alphabet(_config.nextLine());
                _alphabet.characters.toUpperCase();

                if (_config.hasNextInt()) {
                    int rotorCount = _config.nextInt();
                    rotors = rotorCount;

                    if (_config.hasNextInt()) {
                        int pawlCount = _config.nextInt();
                        pawls = pawlCount;
                    }
                    if (_config.hasNext(".+")) {
                        while (_config.hasNext(".+")) {
                            rotorArrayList.add(readRotor());
                            if (rotorArrayList.get(rotorArrayList.size() - 1).reflecting()) {
                                if (rotorArrayList.size() != 1){
                                    throw new EnigmaException("Improper reflector placement");
                                }
                            }
                            if (!(rotorArrayList.get(rotorArrayList.size() - 1).reflecting())){
                                if (rotorArrayList.size() == 1) {
                                    throw new EnigmaException("First rotor must be reflector");
                                }

                            }
                        }
                    }
                } else {
                    throw new EnigmaException("invalid configuration");
                }
            } else {
                throw new EnigmaException("invalid configuration");
            }
            Machine machineNew = new Machine(_alphabet, rotors, pawls, rotorArrayList);
            return machineNew;

        } catch (Exception e) {
            throw new EnigmaException("invalid configuration");
        }
    }

    /** Return a rotor, reading its description from _config.
     * Read cycles as needed to proper format, then check what type of rotor, and read to a new permutation accordingly.
     *
     * */
    private Rotor readRotor() {
        String rotor;
        String notches;
        String cycles;
        String nextString;
        try {
            rotor = _config.next().toUpperCase();
            nextString = _config.next();
            notches = "";
            cycles = "";

            while(_config.hasNext("\\s*[(].+[)]\\s*")) {
                cycles = cycles.concat(_config.next()).concat(" ");
            }

            if (nextString.charAt(0) == "M".charAt(0)) {
                notches = notches.concat(nextString).substring(1, nextString.length());
                return new MovingRotor(rotor , new Permutation(cycles, this._alphabet), notches);
            } else if (nextString.charAt(0) == "N".charAt(0)) {
                return new FixedRotor(rotor, new Permutation(cycles, this._alphabet));
            } else {
                return new Reflector(rotor, new Permutation(cycles, this._alphabet));
            }


        } catch (Exception e) {
            throw new EnigmaException("invalid rotors");
        }
    }

    /** Set M according to the specification given on SETTINGS,
     *  which must have the format specified in the assignment. */
    private void setUp(Machine M, String settings) {
        String[] settingStringArray = settings.split(" ");
        String plugboard = "";
        String[] rotorStringArray = new String[M.numRotors()];
        int k = 1;
        String cur = settingStringArray[0];
        if (cur.charAt(0) == "*".charAt(0)) {
            for (int i = 0 ; i < M.numRotors(); i++) {
                cur = settingStringArray[1 + i];
                rotorStringArray[i] = cur;
                k++;
            }
            M.insertRotors(rotorStringArray);
            M.setRotors(settingStringArray[k]);

            k++;
            for (int p = k; p < settingStringArray.length; p++) {
                plugboard = plugboard.concat(settingStringArray[p] + " ");
            }
            if (plugboard != "") {
                String plugboardString = plugboard.substring(0, plugboard.length() - 1);
                M.setPlugboard(new Permutation(plugboardString, _alphabet));
            }
            else {
                M.setPlugboard(new Permutation("", _alphabet));
            }
        }
    }

    /** Return true iff verbose option specified. */
    static boolean verbose() {
        return _verbose;
    }

    /** Print MSG in groups of five (except that the last group may
     *  have fewer letters). */
    private void printMessageLine(String msg) {
        String message = msg;
        if (message == "") {
            _output.println();
        }
        while (message != "") {
            if (message.length() > 5) {
                _output.print(message.substring(0, 5).concat(" "));
                message = message.substring(5);
            } else {
                _output.println(message);
                message = "";
            }
        }
    }

    /** Alphabet used in this machine. */
    private Alphabet _alphabet;

    /** Source of input messages. */
    private Scanner _input;

    /** Source of machine configuration. */
    private Scanner _config;

    /** File for encoded/decoded messages. */
    private PrintStream _output;

    /** True if --verbose specified. */
    private static boolean _verbose;


}

package enigma;

import org.checkerframework.checker.units.qual.A;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import static enigma.EnigmaException.*;

/** Represents a permutation of a range of integers starting at 0 corresponding
 *  to the characters of an alphabet.
 *  @author
 */
class Permutation {

    /** Set this Permutation to that specified by CYCLES, a string in the
     *  form "(cccc) (cc) ..." where the c's are characters in ALPHABET, which
     *  is interpreted as a permutation in cycle notation.  Characters in the
     *  alphabet that are not included in any cycle map to themselves.
     *  Whitespace is ignored. */
    Permutation(String cycles, Alphabet alphabet) {
        _alphabet = alphabet;
        _permutedAlphabet = alphabet;
        _permutationHashMap = alphabet().toHashMap();
        _oneCycleCleanString = new String();
        _cyclesArrayList = new ArrayList<String>();

        String cyclesClean = cycles.replaceAll("\\s. ", "" );
        cyclesClean = cyclesClean.replaceAll(" ", "");

        int cycleNum = -1;

        for (int i = 0; i < cyclesClean.length(); i++) {
            if (cyclesClean.charAt(i) == "(".charAt(0)) {
                cycleNum++;
                _cyclesArrayList.add("");
            }
            else if (!(cyclesClean.charAt(i) == "(".charAt(0) || cyclesClean.charAt(i) == ")".charAt(0)))  {
                _cyclesArrayList.set(cycleNum, _cyclesArrayList.get(cycleNum).concat(Character.toString(cyclesClean.charAt(i))));
            }
        }

        //add all cycles to generate permutation's hash map
        for (int i = 0; i < _cyclesArrayList.size(); i++ ) {
            addCycle(_cyclesArrayList.get(_cyclesArrayList.size() - 1 - i));
            _permutationHashMap = _permutedAlphabet.toHashMap();
        }
    }

    /** Add the cycle c0->c1->...->cm->c0 to the permutation, where CYCLE is
     *  c0c1...cm. */

   /** Need to convert chars to strings.
    * Individual cycles are stored as ArrayLists of Strings, parens excluded, in an ArrayList: ArrayList<ArrayList<String>>
    */
    private void addCycle(String cycle) {

        int firstPAlphIndexProper = _permutedAlphabet.toInt(cycle.charAt(cycle.length()-1));

        for (int i = 0; i < cycle.length() - 1; i++) {
            char ithCycleChar = cycle.charAt(i);
            char ithImageChar = cycle.charAt(i + 1);

            int[] pHashMapEntry = _permutationHashMap.get(ithImageChar);

            pHashMapEntry[1] = pHashMapEntry[0];
            pHashMapEntry[0] = _permutationHashMap.get(ithCycleChar)[2];

        }
        int[] lastChangeEntry = _permutationHashMap.get(cycle.charAt(0));
        lastChangeEntry[1] = lastChangeEntry[0];
        lastChangeEntry[0] = firstPAlphIndexProper;

        String[] permutedAlphabetStringArr = new String[_alphabet.size()];

        //fill an array with strings, single characters in new permutation order
        for (int j = 0; j < _alphabet.size(); j++){
           int jthAlphaCharNewPos = _permutationHashMap.get(_alphabet.toChar(j))[0];
           permutedAlphabetStringArr[jthAlphaCharNewPos] = String.valueOf(_alphabet.toChar(j));
        }
        String newAlphabetString = String.join("", permutedAlphabetStringArr);
        _permutedAlphabet = new Alphabet(newAlphabetString);
    }


    /** Return the value of P modulo the size of this permutation. */
    final int wrap(int p) {
        int r = p % size();
        if (r < 0) {
            r += size();
        }
        return r;
    }

    /** Returns the size of the alphabet I permute. */
    int size() {
        return _alphabet.size(); // FIXME x
    }

    /** Return the result of applying this permutation to P modulo the
     *  alphabet size. */
    int permute(int p) {
        return _alphabet.toInt(_permutedAlphabet.toChar(p));  // FIXMEx
    }

    /** Return the result of applying the inverse of this permutation
     *  to C modulo the alphabet size. Returns the alphabet index proper. */
    int invert(int c) {
        int p = permute(c);
        int last = p;
        while (!(p == c)) {
            last = p;
            p = permute(p);
        }
        return last;
    }

    /** Return the result of applying this permutation to the index of P
     *  in ALPHABET, and converting the result to a character of ALPHABET. */
    char permute(char p) {
        return _alphabet.toChar(permute(_alphabet.toInt(p)));  // FIXMEx
    }

    /** Return the result of applying the inverse of this permutation to C. */
    char invert(char c) {
        return alphabet().toChar(invert(_alphabet.toInt(c)));
    }

    /** Return the alphabet used to initialize this Permutation. */
    Alphabet alphabet() {
        return _alphabet;
    }

    /** Return true iff this permutation is a derangement (i.e., a
     *  permutation for which no value maps to itself). */
    boolean derangement() {
        for (int i = 0; i < _alphabet.size(); i++) {
            if (_permutedAlphabet.toChar(i) == _alphabet.toChar(i) ) {
                return false;
            }
        }
        return true;

    }

    /** Alphabet of this permutation. */
    private Alphabet _alphabet;
    private Alphabet _permutedAlphabet;
    private HashMap<Character, int[]> _permutationHashMap;
    private ArrayList<String> _cyclesArrayList;
    private String _oneCycleCleanString;
}
